function login() {
    event.preventDefault();

    var email = $("#email").val();
    var pass = $("#password").val();

    var request = {
        url: "/users?filter%5Bwhere%5D%5Bemail%5D=" + email + "&filter%5Bwhere%5D%5Bpass%5D=" + pass,
        obj: null,
        method: "GET",
        callback: auth
    }
    apiCall(request);
}

function auth(data) {

    if (data.length > 0) {

        localStorage.setItem("username", data[0].name);
        localStorage.setItem("clinic", data[0].name);
        localStorage.setItem("address", data[0].address);
        localStorage.setItem("crm", data[0].crm);
        setCookie('auth', 'yes', 1);
        window.location.href = "index.html";
    } else {

    	showMsg("Usuário ou senha inválidos", "#error_msg", true, true);

    }
}